/* ========================================
 * File Path     : ServoMoteur.h
 * Author        : F.MORELLE
 * Date          : 26/03/22
 * Copyright     : (c) 2022 F.MORELLE
 *-----------------------------------------
 */

#ifndef SERVOMOTEUR_H_
#define SERVOMOTEUR_H_

void servoMoteurInit(void);

void servoMoteurCmd(unsigned int cmd);

void servoMoteurDisable(void);

#endif /* SERVOMOTEUR_H_ */
