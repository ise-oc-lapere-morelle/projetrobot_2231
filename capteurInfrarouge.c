/* ========================================
 * File Path     : capteurInfrarouge.c
 * Author        : F.MORELLE
 * Date          : 26/03/22
 * Copyright     : (c) 2022 F.MORELLE
 *-----------------------------------------
 */
 
#include "msp430.h"

/*
 * init_capteurInfrarouge
 */
void init_capteurInfrarouge()
{
// configuration de: Vmax = Vcc et Vmin = 0V;  la duree d'échantillonnage à 4 cycles;
ADC10CTL0= SREF_0 | ADC10SHT_0;
}

/*
 * declenchement de la mesure
 *
*/
int mesure_CI()
{
    //mesure de la tension sur le capteur infrarouge
    ADC10CTL0= ADC10ON;           //Allumer ADC
    //attacher le CAN sur p1.3; choix de ADCSC comme source de declenchement de la conversion;
    //Diviseur d'horloge reglee 1;
    // choix de SMLK comme horloge; mesure simple sur un canal
    ADC10CTL1= INCH_3 |SHS_0 | ADC10DIV_0 | ADC10SSEL_3 | CONSEQ_0;
    ADC10AE0 |= 0b00001000;                   // definition de p1.1 en tant que entree analogique
    ADC10CTL0 |= ENC | ADC10SC;             // debut de la conversion
    while (ADC10CTL1 & ADC10BUSY) {};       //on attend la fin de la conversion
    int p=ADC10MEM;
    ADC10CTL0 &= ~ENC;                     // Conversion finie alors Disable ADC conversion
    return p;
}
