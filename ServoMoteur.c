/* ========================================
 * File Path     : ServoMoteur.c
 * Author        : F.MORELLE
 * Date          : 26/03/22
 * Copyright     : (c) 2022 F.MORELLE
 *-----------------------------------------
 */
 
#include <msp430g2231.h>

/*
 * FONCTION D'INITIALISATION DU TIMER POUR LE SERVOMOTEUR
 * servomoteur sur TA0.1
 */
void servoMoteurInit()
{
    P2SEL |= BIT6; // port P2.6 output timer  (P2.6/TA0.1)
    P2DIR |= BIT6; //
    
    TACTL &= ~MC_0; // arret du timer
    TACCR0 = 33333; // periode du signal PWM 30Hz    33.3ms
    // select TimerA source SMCLK, set mode to up-counting
    TACTL = (TASSEL_2 | MC_1 | ID_0 );
    TACCTL1 = 0 | OUTMOD_7; // select timer compare mode
}

void servoMoteurDisable(void)
{
    //P1SEL &= ~BIT2; // port P1.2 output timer  (P1.2/TA0.1
    //P1DIR &= ~BIT2; // P1.2 en sortie
    P2SEL &= ~BIT6; // port P2.6 output timer  (P2.6/TA0.1)
    P2DIR &= ~BIT6; //
}


/*
 * COMMANDE DU SERVOMOTEUR
 * TACCR1 1ms a 2ms (1000 � 2000) -45�(gauche) a + 45�(droite)
 * point milieu � 1.5ms (1500)
 *
 */
void servoMoteurCmd(unsigned int cmd)
{
    TACCR1 = cmd;
}
