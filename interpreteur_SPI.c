/* ========================================
 * File Path     : interpreteur_SPI.c
 * Author        : F.MORELLE
 * Date          : 26/03/22
 * Copyright     : (c) 2022 F.MORELLE
 *-----------------------------------------------------------------------------
 *
 *carte secondaire du projet SamBOT
 * reception SPI en slave
 * Pilotage capteur ultrason, infrarouge
 * Pilotage servomoteur
 *
 * Launchpad 1.5 / with MSP430G2231
 *------------------------------------------------------------------------------
 */
#include <msp430g2231.h>
#include <string.h>
#include <ServoMoteur.h>
#include <capteurInfrarouge.h>

#define CMDLEN  12               // longueur maximum de la commande utilisateur
#define LF      0x0A
#define CR      0x0D
#define BS      0x7F
#define DELAY_SERVO 500000
#define MEASUREMENT_PERIOD 3500
#define TRIGGER_LENGHT 10
#define AUTO 0x01
#define STOP 0x00


unsigned char c;    //caractere recu
unsigned char d;    //caractere a envoyer
char  cmd[CMDLEN];      // tableau de caractere lie a la commande user
unsigned int   nb_car;           // compteur nombre carateres saisis

int mesure_IR;

unsigned char etat;   //pour machine d'etat

char i_capt;
int tab_pos[3]= { 1000, 1500, 2000};  // nb de position pour le servomoteur
char obstacle[4] = {0x00, 0x10, 0x20, 0x30};
unsigned int t1;
unsigned int t2;
unsigned char distance;

/*
 * definition des sorties LED
 */
void Init_IO( void )
{
   // GPIO - led 1 sur P1.0
   P1SEL &= ~(BIT0 ); //configuration du port P1.0 en digital IO
   P1DIR |= BIT0;  // port 1.0  en sortie
   P1OUT &= ~(BIT0 );  // force etat bas P1.0  - LED D1
   P1REN &= ~(BIT0 );  // pas de resistance sur P1.0
   // GPIO initialisation
    P2SEL &= ~(BIT7); // US-SRF Trigger Line out  P2.7
    P2DIR |= (BIT7);
    P2OUT &= ~BIT7;
}


/* ----------------------------------------------------------------------------
 * Fonction d'initialisation du TIMER Ultra son
 * Entree : -
 * Sorties: -
 */
 void init_TIMER_US( void )
 {
 // Init entree echo
 P1SEL |= BIT2; // US-SRF Echo line input capture P1.2
 P1DIR &= ~BIT2; //TA0.CCI1A
 TACTL &= ~MC_0; // arret du timer
 TACTL = TASSEL_2 | ID_0 | MC_2 | TACLR; // SMCLK; continuous mode, 0 -->FFFF
 TACCR0 = MEASUREMENT_PERIOD - 1;
 TACCTL1 = CM_3 | CAP | CCIS_0 | SCS | CCIE; // falling edge & raising edge,capture mode
 // capture/compare interrupt enable
 // P1.2 (echo) is connected to CCI1A (capture/compare input )
 TACCTL1 &= ~CCIFG;
 }

 void disable_TIMER_US()
 {
 P1SEL &= ~BIT2; // US-SRF Echo line input capture P1.2
 P1DIR |= BIT2;  //TA0.CCI1A
 }


/*
 * Generation du pulse trigger pour capteur ultra son sur P2.7
 *
 */
void send_Trigger_US(void)
{
int i = 0;
for (i=0;i<=TRIGGER_LENGHT;i++)
    {
    P2OUT |= BIT7;
    }
P2OUT &= ~BIT7;
}


/*
 * Initialisation de l'USI en SPI slave
 */
void InitSPIslave(void)
{
// Select SPI Slave CS sur P1.4 (active Low)
P1REN |= BIT4;        //resistor enable
P1OUT |= BIT4;     // pulled up
P1DIR &= ~BIT4;     // P1.4 Input

P1SEL |= (BIT5 | BIT6 | BIT7);  //Secondary peripheral module function is selected

USICTL0 |= USISWRST;  //USI software reset
USICTL1 = 0;
// enable des ports de la fonction SPI, LSB first
USICTL0 |= (USIPE7 | USIPE6 | USILSB | USIPE5);
USICTL0 |= (USIOE | USIGE);
USICTL0 &= ~( USIMST ); // slave

USICTL1 |= USIIE;
USICTL1 &= ~( USII2C | USICKPH); // I2C disable

USICKCTL = 0;           // No Clk Src in slave mode
USICKCTL &= ~(USICKPL | USISWCLK);  // Polarity - Input ClkLow

USICNT = 0;
USICNT &= ~(USI16B | USIIFGCC ); //on utilise seulement 8bit ( USISRL )

P1OUT |= BIT0;   //test    pour instrumenter le passage du chipselect � 0
// attente du CS (chip select)
while (P1IN & BIT4)
{
// attente du passage � 0 du CS
}
P1OUT &= ~BIT0;  //test    pour instrumenter le passage du chipselect � 0
USICTL0 &= ~USISWRST;    //  --USI realeased for operation--

USICNT = 0x08;      // 8 bits count, that re-enable USI for next transfert
}

/*
 * Fonction envoie reception
 */
void RXdataSPI(unsigned char *c, const unsigned char *d)
{
// on attend la reception d'un caractere
while ((USICTL1 & USIIFG) == 0)
{
// waiting flag USI
}
*c = USISRL;   // on recupere le caractere dans la variable c
USISRL = *d;    //load register to send to master
USICNT = 0x08;      // 8 bits count, that re-enable USI for next transfert
}


/*
 * interpretation des commandes et actions
 */
void command( char *cmd )
{
  if (strcmp(cmd, "auto") == 0)
    {
      etat = AUTO;
    }
  else if (strcmp(cmd, "stop") == 0)
    {
      etat = STOP;
    }
}

/*
 * main du programme interpreteur_SPI.c
 */
void main(void)
{
    WDTCTL = WDTPW + WDTHOLD;   // Stop WDT
    // clock calibration verification
    if (CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
    {
       //__low_power_mode_4();
       // factory calibration parameters
       DCOCTL = 0;
    }
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    Init_IO(); // initialisation des sorties
    init_capteurInfrarouge();
    InitSPIslave();
    //__bis_SR_register(LPM0_bits | GIE); // general interrupts enable & Low Power Mode
    __bis_SR_register(GIE);

    i_capt=1;  //position initiale pour le servomoteur
    etat = STOP;

    while (1)
    {
        // gestion de la mesure Infra rouge
        mesure_IR = mesure_CI();
        if (mesure_IR < 0x19E)
        {
            distance = 0x0F;   // obstacle proche
        }
        else
        {
            distance = 0x00;   // obstacle loin
        }
        // cree registre avec position et distance de l'obstacle
        // 4 bits poid fort->position, 4 bits poids faible -> distance
        obstacle[3]= (3 <<4) | distance;

        switch (etat)
        {
           case AUTO  :
               disable_TIMER_US();
               servoMoteurInit();
               servoMoteurCmd(tab_pos[i_capt]);
               __delay_cycles(DELAY_SERVO);
               servoMoteurDisable();
               send_Trigger_US();
               init_TIMER_US();
               __delay_cycles(DELAY_SERVO);
               i_capt++;
               if (i_capt >= 3)
               {
                   i_capt = 0;
               }
              break;

           case STOP  :
              break;

           default:
              i_capt = 0;
              break;
        }
    }
}


// --------------------------- R O U T I N E S   D ' I N T E R R U P T I O N S

/* ************************************************************************* */
/* VECTEUR INTERRUPTION USI                                                  */
/* ************************************************************************* */


#pragma vector= USI_VECTOR
/* cppcheck-suppress unusedFunction ; La fonction est appel� par l'interruption */
__interrupt void universal_serial_interface(void)
{

        d=obstacle[nb_car];
        // d=0x23; //pour test
        RXdataSPI(&c, &d); // on recoit un caractere (et on envoi d)
        cmd[nb_car]=c; //on met ce caractere dans le tableau
        if (c == CR ) // si le caractere CR a ete trouve
        {
           cmd[nb_car]=0x00; //j efface le dernier caractere
           //on execute la partie interpretation des commandes et execution des actions
           command(cmd);
           nb_car=0;  // reinit du nb de caractere recu
        }
        else // pour tous les autres caracteres
        {
           nb_car++; // on incremente le nombre de caractere (index tableau)
        }
}

 /* ************************************************************************* */
 /* VECTEUR INTERRUPTION TIMER */
 /* ************************************************************************* */
 
 
 #pragma vector = TIMERA1_VECTOR
 /* cppcheck-suppress unusedFunction ; La fonction est appel� par l'interruption */
 __interrupt void timer_A_isr(void)
 {
 switch (_even_in_range(TA0IV, TA0IV_TAIFG))
 {
    case TA0IV_NONE:
       break; // no more interrupts pending, exit ISR here.

    case TA0IV_TACCR1: // CCR1 interrupt - 0x0002
    {
       if ( P1IN & BIT2 ) //bit --> entr�e echo
       {
       t1 = TACCR1;
       }
       else
       { // and falling edge
       t2 = TACCR1;
           if (t2 > t1)
           {
               distance=(char)((t2-t1)/1470);  //pas de 25cm  donc on couvre 4m sur 4bits
               if (distance > 0x0F)
               {
                   distance = 0x0F;   // pour eviter le d�passement de 4 bits (distance max)
               }
        // cree registre avec position et distance de l'obstacle
        // 4 bits poid fort->position, 4 bits poids faible -> distance
        obstacle[i_capt]= (i_capt <<4) | distance;
           }
           else
           {
           // -->Pb je ne rafraichi pas les infos capteur
           }
       }
        break;
    }

    case TA0IV_TAIFG: // 0x000A
        break; // timer overflow

    default:
        break;
 }
 TACTL &= ~TAIFG;
 TACCTL1 &= ~CCIFG;
 }
