/* ========================================
 * File Path     : capteurInfrarouge.h
 * Author        : F.MORELLE
 * Date          : 26/03/22
 * Copyright     : (c) 2022 F.MORELLE
 *-----------------------------------------
 */


#ifndef CAPTEURINFRAROUGE_H_
#define CAPTEURINFRAROUGE_H_

void init_capteurInfrarouge();

int mesure_CI();

#endif /* CAPTEURINFRAROUGE_H_ */
